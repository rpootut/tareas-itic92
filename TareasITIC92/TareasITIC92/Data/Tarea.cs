﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TareasITIC92.Data
{
    public class Tarea
    {
        public long Id { get; set; }
        public string Titulo { get; set; }
        public string Detalle { get; set; }
    }
}
